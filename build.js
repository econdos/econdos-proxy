const {compile} = require('nexe')

compile({
  output: 'econdos-cameras',
  build: true,
  target: 'windows-x86-6.11.2',
  ico: './icon.png',
  rc: {
    CompanyName: 'ECONDOS Sistemas',
    PRODUCTVERSION: '1',
    FILEVERSION: '1'
  }
}).then((err) => {
  if (err) throw err
  console.log('success')
})

const {initApi} = require('@econdos/econdos-asterisk-ami');
const { initMip1000IPApi, terminateMip1000IPApi } = require('@econdos/econdos-mip1000');

const host = process.env.HOST || '127.0.0.1';
const httpPort = process.env.PORT || 5050;
const rtspPort = process.env.RTSP_PORT || 5051;
const amiPort = process.env.AMI_PORT || 5052;
const websocketPort = process.env.WEBSOCKET_PORT || 5053;
const mip1000IPPort = process.env.MIP1000IP_PORT || 5054;
const axios = require('axios').default;
const cors_proxy = require('cors-anywhere');
const figlet = require('figlet');
const fs = require('fs')
const express = require('express');
const cors = require('cors');
const app = express();
app.use(cors());
const {proxy, downloaded} = require('rtsp-relay')(app);
const {version} = require('./package.json');
const httpProxy = require('http-proxy');
const http = require('http')

const startProxy = () => {
  cors_proxy.createServer({
    handleInitialRequest: (req, res, location) => {
      if (location && location.host && location.host.endsWith('econdos.com.br') && location.protocol === 'http:') {
        location.protocol = 'https:'
        location.href = location.href.replace('http:', 'https:')
      }
      return false
    },
    httpProxyOptions: {
      secure: false,
      autoRewrite: true,
      followRedirects: true
    }
  }).listen(httpPort, host);
  console.log('Executando o proxy HTTP em http://' + host + ':' + httpPort);
}

const startWebSocketProxy = () => {
  try {
    const wsProxy = new httpProxy.createProxyServer({ secure: false });
    const proxyServer = http.createServer();
    // Recebe a chamada do browser com o ip, porta e path do redirect em base64, faz o parse disso e define os dados do proxy
    proxyServer.on('upgrade', function (req, socket, head) {
      const urlBase64 = req.url.split('?')[1] || '';
      const targetUrl = Buffer.from(urlBase64, 'base64').toString('ascii');
      const isSSL = targetUrl.includes('wss://')
      const targetArray = targetUrl.replace(/ws:\/\/|wss:\/\//, '').split('/')
      const host = targetArray.shift()
      const path = targetArray.join('/')
      const options = { target: `${isSSL ? 'wss' : 'ws'}://${host}` }
      req.url = path
      wsProxy.ws(req, socket, head, options);
    });

    proxyServer.listen(websocketPort);
    console.log('Executando proxy websocket em ws://' + host + ':' + websocketPort);
  } catch (e) {
    console.log('Falha ao iniciar o proxy Websocket');
    console.log(e);
  }
}

const startRtspRelay = () => {
  app.get('/', (req, res) => {
    res.send('O proxy de câmeras RTSP está online');
  })

  // the endpoint our RTSP uses
  app.ws('/api/stream', (ws, req) => {
    const urlBase64 = req.query.url;
    const url = Buffer.from(urlBase64, 'base64').toString('ascii');
    proxy({
      url,
      verbose: false,
      transport: 'tcp',
      // -r define o frame rate do video
      // -b:v define o bit rate do video
      // http://trac.ffmpeg.org/wiki/StreamingGuide
      additionalFlags: ['-strict', '-1', '-r', '15', '-b:v', '1000k', '-bf', '0']
    })(ws)
  })
  try {
    app.listen(rtspPort)
    console.log('Executando o proxy RTSP em http://' + host + ':' + rtspPort);
  } catch (e) {
    console.log('Falha ao iniciar o proxy RTSP');
    console.log(e);
  }
}

const startAsteriskAMI = () => {
  try {
    initApi();
    console.log('Executando integração AMI em http://' + host + ':' + amiPort);
  } catch (e) {
    console.log('Falha ao iniciar integração AMI');
    console.log(e);
  }
}

const startMip1000IP = () => {
  try {
    initMip1000IPApi(mip1000IPPort);
    console.log('Executando integração MIP1000IP em http://' + host + ':' + mip1000IPPort);
  } catch (e) {
    console.log('Falha ao iniciar integração MIP1000IP');
    console.log(e);
  }
}

const closeMip100IP = () => {
  try {
    terminateMip1000IPApi();
    console.log('Encerrando integração MIP1000IP');
  } catch (e) {
    console.log('Falha ao iniciar integração MIP1000IP');
    console.log(e);
  }
}

const exists = fs.existsSync('ffmpeg.exe')
if (!exists) {
  console.log('Baixando arquivos necessários...');
  console.log('Aguarde, por favor...');
}

const showFigletMessage = () => {
  figlet.text('PROXY HTTP e RTSP', (err, data) => {
    if (err) {
      console.log('PROXY');
      console.log('NÃO FECHE ESTA JANELA ENQUANTO ESTIVER UTILIZANDO O SISTEMA');
      return;
    }
    console.log(data);
  });
}

downloaded.then(async () => {
  console.clear();
  showFigletMessage();
  const isRunningHttpProxy = await axios.get(`http://${host}:${httpPort}`).then(() => true).catch(() => false);
  if (!isRunningHttpProxy) {
    startProxy();
  }
  startRtspRelay();
  startAsteriskAMI();
  startWebSocketProxy();
  startMip1000IP();
  console.log(`Proxy versão ${version}`)
  console.log('NÃO FECHE ESTA JANELA ENQUANTO ESTIVER UTILIZANDO O SISTEMA');
})


